#include <stdio.h>
int main()

{
    int y, num;

    printf("Let's factorize the number: \n");
    scanf("%d", &num);

    printf("Factors of %d are: \n", num);

    for(y=1; y<=num; y++)
    {

        if(num % y == 0)
        {
            printf("%d, ",y);
        }
    }

    return 0;
}

#include <stdio.h>
int main()

{
    int y, i, sum = 0;

    printf("Positive integer: \n");
    scanf("%d", &y);

    for (i = 1; i <= y; ++i) {
        sum += i;
    }

    printf("Sum = %d", sum);

    return 0;
}
